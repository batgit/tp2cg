# Treehouse

## Casinha

Execute com:
```bash
$ make compile # compila e roda.
$ make run     # somente roda.
$ make clean   # exclui os arquivos criados.
```

:camel: :boom:

### Controles:
* "w" : Movimento para frente.
* "s" : Mivimento para tras.
* "a" : Movimento para a esquerda.
* "d" : Movimento para a direita.
* "x" : Rotação para a esquerda.
* "X" : Rotação para a direita.
* "y" : Rotação para baixo.
* "Y" : Rotação para cima.
* "p" : Abrir a porta.
* "ESC" : Sair.

### Implementação:
* 70% : Concluido.
* Extras : Não implementados.
