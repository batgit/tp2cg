// Implementação dos métodos da classe vetor.
#include "Vetor.h"
#include "Ponto.h"
#include <math.h>

// Construtores.
Vetor::Vetor(){
    // Vetor irá começar na origem. W por padrão será igual a 0.
    x = y = z = w = 0.0;
}

Vetor::Vetor(double coordX, double coordY, double coordZ){
    x = coordX;
    y = coordY;
    z = coordZ;
    w = 0.0; // W recebe o valor default.
}

// Getters.
double Vetor::getCoordX(){
    return x;
}

double Vetor::getCoordY(){
    return y;
}

double Vetor::getCoordZ(){
    return z;
}

double Vetor::getCoordW(){
    return w;
}

// Setters.
void Vetor::setCoordX(double coordX){
    x = coordX;
}

void Vetor::setCoordY(double coordY){
    x = coordY;
}

void Vetor::setCoordZ(double coordZ){
    x = coordZ;
}

// Métodos da classe.
void Vetor::multiplicaPorEscalar(double escalar){
    x = x * escalar;
    y = y * escalar;
    z = z * escalar;
}

Vetor Vetor::somaVetorComVetor(Vetor v){
    Vetor w;
    double coordX, coordY, coordZ;
    coordX = x + v.getCoordX();
    coordY = y + v.getCoordY();
    coordZ = z + v.getCoordZ();
    w = new Vetor(coordX, coordY, coordZ);
    return w;
}

Vetor Vetor::diferencaVetorComVetor(Vetor v){
    Vetor w;
    double coordX, coordY, coordZ;
    // vetor - v = vetor + (-v)
    coordX = x + ((-1)*v.getCoordX());
    coordY = y + ((-1)*v.getCoordY());
    coordZ = z + ((-1)*v.getCoordZ());
    w = new Vetor(coordX, coordY, coordZ);
    return w;
}

Ponto Vetor::somaPontoComVetor(Ponto p){
    Ponto q;
    double coordX, coordY, coordZ;
    coordX = x + p.getCoordX();
    coordY = y + p.getCoordY();
    coordZ = z + p.getCoordZ();
    q = new Ponto(coordX, coordY, coordZ);
    return q;
}

double Vetor::normaDoVetor(){
    double coordX, coordY, coordZ, norma;
    coordX = x*x; // x ao quadrado.
    coordY = y*y; // y ao quadrado.
    coordZ = z*z; // z ao quadrado.
    norma = sqrt(coordX+coordY+coordZ);
    return norma;
}

Vetor Vetor::vetorNormalizado(){
    double coordX, coordY, coordZ;
    coordX = x / normaDoVetor();
    coordY = y / normaDoVetor();
    coordZ = z / normaDoVetor();
}

double Vetor::produtoEscalar(Vetor w){
    double prodEscalar, coordX, coordY, coordZ;
    coordX = x*w.getCoordX();
    coordY = y*w.getCoordY();
    coordZ = z*w.getCoordZ();
    prodEscalar = coordX+coordY+coordZ;
    return prodEscalar;
}

Vetor Vetor::produtoVetorial(Vetor w){
    // Calculado da forma matricial.
    Vetor resultado;
    double coordX, coordY, coordZ;
    coordX = (y*w.getCoordZ()) - (z*w.getCoordY());
    coordY = (z*w.getCoordX()) - (x*w.getCoordZ());
    coordZ = (x*w.getCoordY()) - (y*w.getCoordX());
    resultado = new Vetor(coordX, coordY, coordZ);
    return resultado;
}

// Ficaremos sem este método por enquanto.
/*
double anguloEntreVetores(Vetor w){
    // Vamos utilizar a forma arcocos(este_vetor*w). Este vetor e w estarão normalizados.
    // O angulo interno é igual ao arccos do produtoEscalar de dois vetores normalizados.
    double resultado = acos(produtoEscalar(Vetor::normaDoVetor()))
}
*/
