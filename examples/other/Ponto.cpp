#include "Vetor.h"
#include "Ponto.h"
#include <math.h>

// Construtores.
Ponto::Ponto(){
    x = y = z = 0;
    w = 1;
}

Ponto::Ponto(double coordX, double coordY, double coordZ){
    x = coordX;
    y = coordY;
    z = coordZ;
    w = 1; // Definição de ponto.
}

// Getters.
double Ponto::getCoordX(){
    return x;
}

double Ponto::getCoordY(){
    return y;
}

double Ponto::getCoordZ(){
    return z;
}

// Setters.
void Ponto::setCoordX(double coordX){
    x = coordX;
}

void Ponto::setCoordY(double coordY){
    y = coordY;
}

void Ponto::setCoordZ(double coordZ){
    z = coordZ;
}

// Métodos da classe.
Ponto Ponto::somaPontoComVetor(Vetor v){
    Ponto resultado;
    double coordX, coordY, coordZ;
    coordX = x + v.getCoordX();
    coordY = y + v.getCoordY();
    coordZ = z + v.getCoordZ();
    resultado = new Ponto(coordX, coordY, coordZ);
    return resultado;
}

Vetor Ponto::diferencaEntrePontos(Ponto p){
    Vetor resultado;
    double coordX, coordY, coordZ;
    coordX = x - p.getCoordX();
    coordY = y - p.getCoordY();
    coordZ = z - p.getCoordZ();
    resultado = new Vetor(coordX, coordY, coordZ);
    return resultado;
}

double Ponto::distanciaEntrePontos(Ponto p){
    Vetor resultado;
    double coordX, coordY, coordZ;
    coordX =  p.getCoordX() - x;
    coordY =  p.getCoordY() - y;
    coordZ =  p.getCoordZ() - z;
    resultado = new Vetor(coordX, coordY, coordZ);
    return resultado;
}
