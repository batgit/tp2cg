#include <iostream>
#include <math.h>
using namespace std;

// Definição da classe.
class Ponto {
    private: // Campos privados.
        double x, y, z, w;
        // Pela definição de ponto. w é constante.

    public: // Ralé.
        // Construtores.
        Ponto();
        Ponto(double coordX, double coordY, double coordZ);
        // Getters.
        double getCoordX();
        double getCoordY();
        double getCoordZ();
        // Setters.
        void setCoordX(double coordX);
        void setCoordY(double coordY);
        void setCoordZ(double coordZ);
        // Métodos da classe.
        Ponto somaPontoComVetor(Vetor v);
        Vetor diferencaEntrePontos(Ponto p);
        double distanciaEntrePontos(Ponto p);
};
