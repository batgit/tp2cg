// Vamos a nossa classe de vetor.
#include <iostream>
using namespace std;

class Vetor {
    private:
        double x, y, z, w; // Coordenadas.
        // Pela definição de vetor, w será constante.

    public: // Galera publica.
        // Construtores.
        Vetor();
        Vetor(double coordX, double coordY, double coordZ);
        // Getters.
        double getCoordX();
        double getCoordY();
        double getCoordZ();
        double getCoordW();
        // Setters.
        void setCoordX(double coordX);
        void setCoordY(double coordY);
        void setCoordZ(double coordZ);
        // Métodos da classe.
        void multiplicaPorEscalar(double escalar);
        Vetor somaVetorComVetor(Vetor auxiliar);
        Vetor diferencaVetorComVetor(Vetor auxiliar);
        Ponto somaPontoComVetor(Ponto p);
        double normaDoVetor();
        Vetor vetorNormalizado();
        double produtoEscalar(Vetor auxiliar);
        Vetor produtoVetorial(Vetor auxiliar);
        double anguloEntreVetores(Vetor auxiliar);
};
