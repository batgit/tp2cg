#include "Camera.h"

Camera::Camera(){
     init(0.0, 0.0, 7.0, 0.0, 0.0, 0.0);
}

Camera::Camera(double x, double y, double z, double ax, double ay, double az){
    this->eyeX=x;
    this->eyeY=y;
    this->eyeZ=z;
    this->xAngle=ax;
    this->yAngle=ay;
    this->zAngle=az;
}

double Camera::getEyeX(){
    return eyeX;
}

double Camera::getEyeY(){
    return eyeY;
}

double Camera::getEyeZ(){
    return eyeZ;
}

double Camera::getXAngle(){
    return xAngle;
}

double Camera::getYAngle(){
    return yAngle;
}

double Camera::getZAngle(){
    return zAngle;
}

void Camera::setEyeX(double x){
    eyeX = x;
}

void Camera::setEyeY(double x){
    eyeY = x;
}

void Camera::setEyeZ(double x){
    eyeZ = x;
}

void Camera::setXAngle(double x){
    xAngle = x;
}

void Camera::setYAngle(double x){
    yAngle = x;
}

void Camera::setZAngle(double x){
    zAngle = x;
}

void Camera::init(double x, double y, double z, double ax, double ay, double az){
    eyeX = x;
    eyeY = y;
    eyeZ = z;
    xAngle = ax;
    yAngle = ay;
    zAngle = az;
}

void Camera::lookAt(){
    gluLookAt(eyeX, 7.0 , eyeZ, xAngle, yAngle, zAngle, 0.0, 1.0, 0.0);
}

void Camera::rotateAt(double x, double y, double z){
    glRotatef(xAngle, 1.0, 0.0, 0.0); //Rotação no eixo x.
    glRotatef(yAngle, 0.0, 1.0, 0.0); //Rotação no eixo y.
    glRotatef(zAngle, 0.0, 0.0, 1.0); //Rotação no eixo z.
}

void Camera::printData(){
    cout << "x=" << eyeX << " y=" << eyeY << " z=" << eyeZ << endl;
    cout << "ax=" << xAngle << " ay=" << yAngle << " az=" << zAngle << endl;
}
