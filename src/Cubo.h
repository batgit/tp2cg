#ifndef CUBO_H
#define CUBO_H
// Include padrão. Se assemelha ao stdio.
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "Camera.h"
using namespace std;

// Definição da classe.
class Cubo {
    private: // Campos privados.
        double base, altura, profundidade;

    public: // Galera publica.
        // Contrutores.
        Cubo();
        // Getters.
        double getBase();
        double getAltura();
        double getProfundidade();
        // Setters.
        void setBase(double x);
        void setAltura(double y);
        void setProfundidade(double z);
        // Métodos da classe.
        void init(double x, double y, double z);
        double volume();
        void desenhaCubo();
        void printData();
};
#endif
