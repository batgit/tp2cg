#ifndef CAM_H
#define CAM_H
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
using namespace std;

class Camera{
    private:
        GLdouble eyeX, eyeY, eyeZ;
    	GLdouble xAngle, yAngle, zAngle;

    public:
        //Construtor
        Camera();
        Camera(double x, double y, double z, double ax, double ay, double az);
        //Getters
        double getEyeX();
        double getEyeY();
        double getEyeZ();
        double getXAngle();
        double getYAngle();
        double getZAngle();
        //Setters
         void setEyeX(double x);
         void setEyeY(double x);
         void setEyeZ(double x);
         void setXAngle(double x);
         void setYAngle(double x);
         void setZAngle(double x);
         //Métodos da classe. Aqui acontece a brincadeira
         void init(double x, double y, double z, double ax, double ay, double az);
         void lookAt();
         void rotateAt(double x, double y, double z);
         void printData();
};
#endif
