#ifndef MOVE_H
#define MOVE_H
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "Camera.h"
using namespace std;

class MovementIn{
    private:
        int controlaPorta;
    public:
        MovementIn();
        void init();
        void FuncaoTeclado(Camera &c,unsigned char key, int x, int y);
        int getControlaPorta();
        void setControlaPorta(int val);
};
#endif
