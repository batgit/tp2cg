#include "Cubo.h"
Cubo::Cubo(){
    base = altura = profundidade = 0;
}

// Getters.
double Cubo::getBase(){
    return base;
}

double Cubo::getAltura(){
    return altura;
}

double Cubo::getProfundidade(){
    return profundidade;
}

// Setters.
void Cubo::setBase(double x){
    base = x;
}

void Cubo::setAltura(double y){
    altura = y;
}

void Cubo::setProfundidade(double z){
    profundidade = z;
}

// Iniciador dos valores. Aqui você pode brincar kk.
void Cubo::init(double x, double y, double z){
    base = x;
    altura = y;
    profundidade = z;
}

// Método para cálulo do volume.
double Cubo::volume(){
    return altura*base*profundidade;
}

void Cubo::desenhaCubo(){
    // Empilhar matriz de transformação da câmera.
    glPushMatrix();
        // glScalef(2.0, 2.0, 1.0);
        // Definindo a cor para desenho.
        glColor3f(0.0, 0.0, 1.0);
        glTranslatef(0.0, 0.0, 0.0);
        glutSolidCube((double)base*altura);
    glPopMatrix();
}

void Cubo::printData(){
    cout << "base=" << base << " altura=" << altura << " profundidade=" << profundidade << endl;
}
