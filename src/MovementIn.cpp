#include "MovementIn.h"

MovementIn::MovementIn(){
    controlaPorta = 1;
}

void MovementIn::init(){

}

void MovementIn::FuncaoTeclado(Camera &c,unsigned char key, int x, int y){
    c.printData();
    switch (key) {
        case 27:
            exit(0);
            break;
        //Movimenta o personagem.
        case 'a':
            c.setEyeX(c.getEyeX()-1.2);
            c.setXAngle(c.getXAngle()-1.2);
            break;
        case 'd':
            c.setEyeX(c.getEyeX()+1.2);
            c.setXAngle(c.getXAngle()+1.2);
            break;
        case 's':
            c.setEyeZ(c.getEyeZ()+1.2);
            c.setZAngle(c.getZAngle()+1.2);
            break;
        case 'w':
            c.setEyeZ(c.getEyeZ()-1.2);
            c.setZAngle(c.getZAngle()-1.2);
            break;
        //abre a porta.
        case 'p':
            controlaPorta *= (-1);
            break;
        //Rotaciona a camêra em y.
        case 'Y':
            c.setYAngle(c.getYAngle()+1.9);
                break;
        case 'y':
            c.setYAngle(c.getYAngle()-1.9);
            break;
        //Rotaciona a camêra em x.
        case 'X':
            c.setXAngle(c.getXAngle()+1.9);
            break;
        case 'x':
            c.setXAngle(c.getXAngle()-1.9);
            break;
    }
    glutPostRedisplay();
}

int MovementIn::getControlaPorta(){
  return controlaPorta;
}

void MovementIn::setControlaPorta(int val){
  controlaPorta = val;
}
