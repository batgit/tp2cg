#include <iostream>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <time.h>
#include <math.h>
#include "Cubo.h"
#include "MovementIn.h"
#include "Glm.h"
using namespace std;

Cubo c;
Camera cam(0.0,0.0,60.0,0.0,7.0,0.0);
MovementIn tecladoIn;
int contador=0;
int portinha = 0;

//GLMmodel* objCubo;
GLMmodel* CasaOBJ;
GLMmodel* TelhadoOBJ;
GLMmodel* CemiterioOBJ;
GLMmodel* ChaoOBJ;
GLMmodel* MesaOBJ;
GLMmodel* CadeiraOBJ;
GLMmodel* PenduloOBJ;
GLMmodel* RelogioOBJ;
GLMmodel* PortaOBJ;

void init(){
    // Começando a trabalhar com iluminação.
    // Referência: http://www.inf.pucrs.br/~manssour/OpenGL/Iluminacao.html
    GLfloat luzAmbiente[4] = {0.2, 0.2, 0.2, 1.0};
    GLfloat luzDifusa[4] = {0.7, 0.7, 0.7, 1.0};
    GLfloat luzEspecular[4] = {1.0, 1.0, 1.0, 1.0};
    GLfloat posicaoLuz[4] = {0.0, 50.0, 50.0, 1.0}; // Luz posicional.

    // Capacidade de brilho do material.
    GLfloat especularidade[4] = {1.0, 1.0, 1.0, 1.0};
    GLint especMaterial = 60;
    CemiterioOBJ = glmReadOBJ("../objs/CemiterioOBJ.obj");
    TelhadoOBJ = glmReadOBJ("../objs/TelhadoOBJ.obj");
    CasaOBJ = glmReadOBJ("../objs/CasaOBJ.obj");
    ChaoOBJ = glmReadOBJ("../objs/ChaoOBJ.obj");
    MesaOBJ = glmReadOBJ("../objs/MesaOBJ.obj");
    CadeiraOBJ = glmReadOBJ("../objs/CadeiraOBJ.obj");
    PenduloOBJ = glmReadOBJ("../objs/PenduloOBJ.obj");
    RelogioOBJ = glmReadOBJ("../objs/RelogioOBJ.obj");
    PortaOBJ = glmReadOBJ("../objs/PortaOBJ.obj");
    // Calculando as normais.
    glmFacetNormals(CemiterioOBJ);
    glmFacetNormals(TelhadoOBJ);
    glmFacetNormals(ChaoOBJ);
    glmFacetNormals(CasaOBJ);
    glmFacetNormals(CadeiraOBJ);
    glmFacetNormals(MesaOBJ);
    glmFacetNormals(RelogioOBJ);
    glmFacetNormals(PenduloOBJ);
      glmFacetNormals(PortaOBJ);
    // Fazendo uma coisa que eu não sei o que é.
    glmVertexNormals(CemiterioOBJ, 90.0);
    glmVertexNormals(TelhadoOBJ, 90.0);
    glmVertexNormals(ChaoOBJ, 90.0);
    glmVertexNormals(CasaOBJ, 90.0);
    glmVertexNormals(CadeiraOBJ, 90.0);
    glmVertexNormals(MesaOBJ, 90.0);
    glmVertexNormals(RelogioOBJ, 90.0);
    glmVertexNormals(PenduloOBJ, 90.0);
    glmVertexNormals(PortaOBJ, 90.0);
    // Fazendo a escala.
    glmScale(CemiterioOBJ, 0.4);
    glmScale(TelhadoOBJ, 0.4);
    glmScale(ChaoOBJ, 0.4);
    glmScale(CasaOBJ, 0.4);
    glmScale(CadeiraOBJ, 0.4);
    glmScale(MesaOBJ, 0.4);
    glmScale(RelogioOBJ, 0.4);
    glmScale(PenduloOBJ, 0.4);
    glmScale(PortaOBJ, 0.4);
    // Definindo a cor preta como a cor de Limpeza.
 	glClearColor(0.5, 0.5, 0.5, 0.5);
    // Habilita o modelo de colorização de Gouraud.
    glShadeModel(GL_SMOOTH);

    // Define a refletância do material.
    glMaterialfv(GL_FRONT, GL_SPECULAR, especularidade);
    // Define a concentração do brilho.
    glMateriali(GL_FRONT, GL_SHININESS, especMaterial);

    // Ativa o uso da luz ambiente.
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

    // Define os paramêtros da luz de núemro 0.
    glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa);
    glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular);
    glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz);

    // Habilita a definição da cor material a partir da cor corrente
    glEnable(GL_COLOR_MATERIAL);
    // Habilita o uso de iluminação.
    glEnable(GL_LIGHTING);
    // Habilita a luz de número 0.
    glEnable(GL_LIGHT0);
    // Habilita o depth-buffering.
    glEnable(GL_DEPTH_TEST);
}

// Aqui tem cubo.
void desenha(){
    // Limpando o buffer.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Carregar a matriz de identidade na pilha de matrizes.
    glLoadIdentity();

    cam.lookAt();
    // Empilhar matriz de transformação da câmera.
    glPushMatrix();
    glColor3f(0.2,1.0,0.2);
    glmDraw(ChaoOBJ, GLM_FLAT | GLM_MATERIAL);
    glColor3f(0.0,0.0,0.0);
    glmDraw(CemiterioOBJ, GLM_FLAT | GLM_MATERIAL);
    glColor3f(1.0,1.0,0.3);
    glmDraw(MesaOBJ, GLM_FLAT | GLM_MATERIAL);
    glColor3f(0.9,0.6,0.0);
    glmDraw(CadeiraOBJ, GLM_FLAT | GLM_MATERIAL);
    glColor3f(0.5,1.0,0.7);
    glmDraw(RelogioOBJ, GLM_FLAT | GLM_MATERIAL);
    glColor3f(1.0,0.0,0.0);
    glmDraw(CasaOBJ, GLM_FLAT | GLM_MATERIAL);
    glColor3f(0.0,0.0,1.0);
    glmDraw(TelhadoOBJ, GLM_FLAT | GLM_MATERIAL);
		// c.desenhaCubo();
    glPopMatrix();
      glPushMatrix();
        glTranslatef(10.7,0.0,0.9);
        /* translada primeiro, rotaciona depois.
            o que vai variar nesse rotate é o primeiro parametro,
            que vai variar de 0 a -90.
        */
        if(portinha)
          glRotatef(90.0,0.0,1.0,0.0);
        if(tecladoIn.getControlaPorta() == (-1)){
          glRotatef(sin(contador*3.141592653589793/180)*(-90.0),0.0,1.0,0.0);
          //cout << tecladoIn.getControlaPorta() << endl;
          cout << sin(contador*3.141592653589793/180)*(-90.0) << endl;
          if (sin(contador*3.141592653589793/180)*(-90.0) == (90) || sin(contador*3.141592653589793/180)*(-90.0) == (-90))
          {
            tecladoIn.setControlaPorta(tecladoIn.getControlaPorta() * (-1));
            portinha = 1;
          }
        }
          glmDraw(PortaOBJ, GLM_FLAT | GLM_MATERIAL);
      glPopMatrix();
    glColor3f(1.0,1.0,1.0);
      glPushMatrix();
        glTranslatef(25.0,4.8,-20.9);
        //a variacao do pendulo no rotate ocorrerá somente no primeiro
        //parametro, variando de -10 até 10.
        glRotatef(sin(contador*3.141592653589793/180)*20, 1.0, 0.0, 0.0);
          glmDraw(PenduloOBJ, GLM_FLAT | GLM_MATERIAL);
      glPopMatrix();
    glutSwapBuffers();
}

void reshape(int w, int h){
    // Setando valor default
    if ( h == 0 )
        h = 1;
    // Definir o tamanho da janela no display, em pixels.
    glViewport(0, 0, (GLsizei) w, (GLsizei)h);
    // CHavear para a pilha de transformação de projeção.
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(94.0, (float)w/(float)h, 1.0, 500.0);
    // Chavear para a pilha de transformação de modelo.
    glMatrixMode(GL_MODELVIEW);
}

void keyboard(unsigned char key, int x, int y){
    tecladoIn.FuncaoTeclado(cam,key,x,y);
    glutPostRedisplay();
}

void getTempoGame(int x){
    contador++;
    //cout << contador << endl;
    int fps =60;
    glutPostRedisplay();
    glutTimerFunc(1000.0/fps,getTempoGame,0);
}

int main(int argc, char** argv){
	cam.printData();
    c.init(1.0,1.0,1.0);
	c.printData();
    tecladoIn.init();
    glutInit(&argc, argv);
    glutTimerFunc(20,getTempoGame,1);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(800, 700);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Um Cubo, Gracas a Deus");
    // glutFullScreen();
    // Inicilizar o openGL.
    init();
    // Registrar os tratadores de eventos.
    glutDisplayFunc(desenha);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutPostRedisplay();
    glutTimerFunc(0,getTempoGame,0);
    // Entra em modo de espera.
    glutMainLoop();
    return 0;
}
